#include <stdio.h>
int main() {
    char l;
    int simple_vowels, capital_vowels;
    printf("Enter an English Letter: ");
    scanf("%c", &l);

    simple_vowels = (l == 'a' || l == 'e' || l == 'i' || l == 'o' || l == 'u');
    capital_vowels = (l == 'A' || l == 'E' || l == 'I' || l == 'O' || l == 'U');

    if (simple_vowels || capital_vowels)
        printf("%c is a Vowel.", l);
    else
        printf("%c is a Consonant.", l);

    return 0;
}
